//after the page loads fully->
window.addEventListener('load', () =>{
    console.log("Page Loaded");
    
    //storing form data from the page to variables
    const formData = document.querySelector('#new-form');
    //storing input task field data
    const inputTaskData = document.querySelector('#input-task-field'); 
    //storing delete button info if clicked
    const deleteAllButton = document.querySelector('#delete-all-bttn');
    //storing list of all tasks
    const list_ele = document.querySelector('#list-all-tasks');

//not working properly
// deleteAllButton.addEventListener("click", (e) =>{
//     const myNode = document.getElementById("#list-all-tasks");
//     while (myNode.firstChild) {
//         myNode.removeChild(myNode.lastChild);
//     }
// });

    //after submit button is clicked
    formData.addEventListener("submit" , (e) =>{

        e.preventDefault(); //preventing loading of page

        //getting value user put in field and storing in variable
        const task_text = inputTaskData.value; 

        //checking if field is empty
        if(!task_text){
            alert("A task cannot be empty. Please fill your task details.");
            return;
        }else{
            console.log("Task added");
        }
        //
        //FOR DISPLAYING TASKS IN LIST:
        //

        //creating div component for whole task and buttons
        const indvl_task_ele = document.createElement("div");
        //adding class name to div component tag
        indvl_task_ele.classList.add("indvl-task"); 

        //creating div component for content part
        const task_content_ele = document.createElement("div");   
        //adding class name to div component tag
        task_content_ele.classList.add("task-content");


        //making task_content element child of indvl_task element
        indvl_task_ele.appendChild(task_content_ele);

        //creating input component for task text
        const task_input_ele = document.createElement("input");
        //adding class name to input component tag 
        task_input_ele.classList.add("task-text");
        //declaring type of input as text
        task_input_ele.type = "text";
        //declaring value to display as text in task list
        task_input_ele.value = task_text;

//not working
//capitalizing task text
// task_input_ele.style = "text-tranform: capitalize";

        //setting text value to read-only
        task_input_ele.setAttribute("readonly", "readonly");

        //making task_input element child of task_content element
        task_content_ele.appendChild(task_input_ele);

        //creating div component for action buttons
        const task_actions_ele = document.createElement("div");
        //adding class name to div component
        task_actions_ele.classList.add("indvl-task-actions");

        //creating button component for edit button
        const task_edit_ele = document.createElement("button");
        //adding class name to button component
        task_edit_ele.classList.add("task-edit-button");
        //adding text to be shown on button
        task_edit_ele.innerText = "Edit";

        //creating button component for delete button
        const task_delete_ele = document.createElement("button");
        //adding class name to button component
        task_delete_ele.classList.add("task-delete-button");
        //adding text to be shown on button
        task_delete_ele.innerText = "Delete";

        //making both buttons child of actions element
        task_actions_ele.appendChild(task_edit_ele);
        task_actions_ele.appendChild(task_delete_ele);
        
        //making task_actions element child of indvl_task element
        indvl_task_ele.appendChild(task_actions_ele);

        //making indvl_task element child of list element
        list_ele.appendChild(indvl_task_ele);

        //making input value empty
        inputTaskData.value = "";

        //after edit button is pressed
        task_edit_ele.addEventListener('click', () =>{
            //checking if button shows edit or save
            if(task_edit_ele.innerText == "Edit"){
                //removing read-only attribute to edit the text
                task_input_ele.removeAttribute("readonly");
                //making cursor focus on text
                task_input_ele.focus();
                //changing button text to save 
                task_edit_ele.innerText = "Save";
            }else{ //if save is shown
                //changing attribute to read-only again to save
                task_input_ele.setAttribute("readonly", "readonly");
                //change button text to edit 
                task_edit_ele.innerText = "Edit";
                console.log("Changes Saved Successfully.");
            }
        });

        //after delete button is pressed
        task_delete_ele.addEventListener('click', () =>{
            //removing child i.e. indvl task from the list
            list_ele.removeChild(indvl_task_ele);
            console.log("Task Deleted Successfully");
        });
    });


});